FROM ubuntu:20.04

RUN apt-get update && apt-get install --no-install-recommends -y \
    g++ \
    clang-tidy-12 \
    python3-pip \
    make \
    git \
    && rm -rf /var/lib/apt/lists/* \
    && python3 -m pip install cmake

RUN update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-12 120
