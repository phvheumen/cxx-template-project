function(target_apply_options target)
    if(STATIC_ANALYSIS)
        set(CXX_STATIC_ANALYZER clang-tidy)
    if(WARNING_AS_ERROR)
        set(CXX_STATIC_ANALYZER ${CXX_STATIC_ANALYZER} --warnings-as-errors=*)
    endif()
    set_target_properties(${target} PROPERTIES CXX_CLANG_TIDY "${CXX_STATIC_ANALYZER}")
    endif()

    target_compile_options(${target} 
    PRIVATE 
        -Wall 
        -Wextra 
        $<$<BOOL:${WARNING_AS_ERROR}>:-Werror>
    )
endfunction()