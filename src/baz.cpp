#include "baz.hpp"

void Baz::set(int i, float f) {
    this->_int_float_pair = {i, f};
}

void Baz::set_square(int i, float f) {
    this->_int_float_pair = {i*i, f*f};
}

int Baz::get_integer() {
    return std::get<0>(_int_float_pair);
}