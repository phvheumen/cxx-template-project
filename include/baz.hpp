#include <tuple>
#include "baz_export.h"

class BAZ_EXPORT Baz {
public:
    Baz() {};
    ~Baz() {};

    void set(int i, float f);
    void set_square(int i, float f);
    int get_integer();


private:
    std::tuple<int, float> _int_float_pair;
};