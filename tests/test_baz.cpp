#include <gtest/gtest.h>
#include <baz.hpp>

TEST(testBaz, retrieveIntegerValue) {
    Baz baz;
    baz.set_square(2, 2.0f);
    EXPECT_EQ(baz.get_integer(), 4);
}